const mongoose = require("mongoose");
const config = require("config");
const articles = require("./routes/article");
const category = require("./routes/category");
const users = require("./routes/users");
const auth = require("./routes/auth");
const upload = require("./routes/upload")
const express = require("express");
const Joi = require("@hapi/joi");
Joi.objectId = require("joi-objectid")(Joi);
const app = express();
if (!config.get("jwtPrivateKey")) {
  console.error("FATAL ERROR: jwtPrivateKey is not defined.");
  process.exit(1);
}

mongoose
  .connect("mongodb://localhost/magazine")
  .then(() => console.log("Connected to MongoDB..."))
  .catch(err => console.log(`Couldn't connect to MongoDB: ${err}`));

app.use(express.json());
app.use("/api/article", articles);
app.use("/api/category", category);
app.use("/api/user", users);
app.use("/api/auth", auth);
app.use("/api/upload",upload)

const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Listening on port ${port}`));
