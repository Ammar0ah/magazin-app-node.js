const express = require("express");
const { Category, validate } = require("../models/category");
const auth = require("../middlewares/auth");
const admin = require("../middlewares/admin")
const router = express.Router();

router.post("/",[auth,admin], async (req, res) => {
  const { error } = validate(req.body);

  if (error) return res.status(400).send(error.details[0].message);

  const { name } = req.body;
  const isExisted = await Category.findOne({ name });
  if (isExisted) return res.status(400).send("Category is already existed.");

  let category = new Category({ name });
  try {
    category = await category.save();
    res.send(category);
  } catch (err) {
    res.sendStatus(500);
    console.error(err.message);
  }
});

router.get("/",async (req, res) => {
  const categories = await Category.find().select("name");
  res.send(categories);
});

router.put("/:id",[auth,admin], async (req, res) => {
  const { id } = req.params;
  const { name } = req.body;

  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  if (!id) {
    return res.status(400).send("Missing Id");
  }
  const result = await Category.update(
    { _id: id },
    {
      $set: {
        name: name
      }
    }
  );
  res.send(result);
});

router.delete("/:id",[auth,admin], async (req, res) => {
  const { id } = req.params;
  if (!id) {
    return res.status(400).send("Missing Id");
  }
  const result = await Category.findOneAndDelete({ _id: id });
  if (!result) return res.sendStatus(404);

  res.send("One Item Deleted Successfully");
});

module.exports = router;
