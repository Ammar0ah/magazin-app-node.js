const uuidv1 = require("uuid/v1");
const express = require("express");
const router = express.Router();
const multer = require("multer");
const { Article } = require("../models/article");
const path = require("path");
const auth = require('../middlewares/auth');
const admin = require('../middlewares/admin');
let filename = ""
const storage = multer.diskStorage({
  destination: "uploads/",
  filename: function(req, file, cb) {
      filename = uuidv1() + path.extname(file.originalname);
    cb(null,filename);
  }
});

const upload = multer({
  storage: storage,
  fileFilter: function(req, file, cb) {
    checkFileType(file, cb);
  }
}).single("image");
/*
    article_id
*/
const checkFileType = (file, cb) => {
  const extname = /jpg/.test(path.extname(file.originalname).toLowerCase());
  const mimeType = /jpg/.test(file.mimeType);
  if (extname && mimeType) {
    cb(null, true);
  } else {
    cb("Error : jpg only");
  }
};
// I Can post and put in the same Route ... //
router.post("/:id",[auth,admin], async (req, res) => {
    upload(req,res,(err) => {
        if(err){
            console.log(err)
        }
    })
  let  article_id  = req.params.id;
  let article = await Article.findById(article_id);
  if (!article) return res.sendStatus(404);
  article.photo = filename
  article = await article.save();
  res.send(article)
});

module.exports = router;
