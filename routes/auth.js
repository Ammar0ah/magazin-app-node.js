const mongoose = require("mongoose");
const express = require("express");
const jwt = require("jsonwebtoken");
const Joi = require("@hapi/joi");
const bcrypt = require("bcrypt");
const _ = require("lodash");
const { User, validate } = require("../models/user");
const router = express.Router();

router.post("/", async (req, res) => {
  const { error } = validateLogin(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const { email, password } = req.body;
  let user = await User.findOne({ email });
  if (!user) res.status(400).send("Invalid email or password");

  let isValidpassword = await bcrypt.compare(password, user.password);
  if (!isValidpassword) res.status(400).send("Invalid email or password");

  const token = user.generateAuthToken();
  res.send(token);
});

const validateLogin = user => {
  const schema = Joi.object({
    email: Joi.string()
      .email({ minDomainSegments: 2, tlds: [] })
      .required(),
    password: Joi.string()
      .required()
      .min(8)
      .max(255)
  });
  return schema.validate(user);
};

module.exports = router;
