const _ = require("lodash");
const express = require("express");
const auth = require("../middlewares/auth");
const admin = require("../middlewares/admin");
const {validate,Article} = require("../models/article");
const fs = require('fs')

const router = express.Router();
/* 
    name,
    content,
    category_id,
    photo
*/
router.post('/',[auth,admin],async(req,res)=>{
    const {error} = validate(req.body);
    const {title} = req.body
    if(error)
       return res.status(400).send(error.details[0].message);
    let article = await Article.findOne({title});
    if(article) 
       return res.status(400).send('Duplicated, Please pick another title');
    article = new Article(_.pick(req.body,['title','content','category_id']));
    article = await article.save();
    res.send(article)
})

router.get('/',async(req,res)=>{
    const articles = await Article.find();
    res.send(articles);
})

router.get('/:id',async(req,res)=>{
    const {id} = req.params;
    let article = await Article.findById(id);
    if(!article)
        return res.sendStatus(404);
    res.send(article)
})

router.delete('/:id',[auth,admin] , async(req,res) => {
    let {id} = req.params;

    let article =  await Article.findByIdAndDelete(id);
    console.log(article)
    if(!article) return res.sendStatus(404)
    let uuid = article.photo;
    const path = '../uploads/' + uuid
    console.log(path)
    try {
        await fs.unlink(path)
    } catch (error) {
        console.log(error.message)
    }
    res.sendStatus(200)
});

module.exports = router;
