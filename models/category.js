const mongoose = require('mongoose');
const Joi = require('@hapi/joi')

const categorySchema = mongoose.Schema({
    name:{
        type:String,
        required:true,
        minlength:5,
        maxlength:20
    }
})
const Catogery = mongoose.model('Category',categorySchema);
const validateCategory = (req) => {
    const schema = Joi.object({
        name: Joi.string().min(3).max(20).required(),
    })
    return schema.validate(req);
}

exports.Category = Catogery;
exports.validate = validateCategory;