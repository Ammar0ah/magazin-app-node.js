const Joi = require("@hapi/joi")

const mongoose = require("mongoose");

const articleSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
    min: 3,
    max: 255
  },
  content: {
    type: String,
    required: true,
    min: 5,
    max: 1024
  },
  photo:{
    type:String,
  },
  category_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Category",
    required: true
  }
});
const Article = mongoose.model("Article", articleSchema);
const validateArticle = req => {
  const schema = Joi.object({
    title: Joi.string()
      .min(3)
      .max(255)
      .required(),

    content: Joi.string()
      .min(5)
      .max(1024)
      .required(),

    category_id: Joi.objectId().required(),

  });
  return schema.validate(req);
};

exports.validate = validateArticle;
exports.Article = Article
