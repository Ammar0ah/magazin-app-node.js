const mongoose = require("mongoose");
const Joi = require("@hapi/joi");
const jwt = require("jsonwebtoken");
const config = require("config");
const userSchema = mongoose.Schema({
  username: {
    type: String,
    required: true,
    min: 3,
    max: 20
  },
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true,
    min: 8,
    max: 255
  },
  isAdmin: Boolean
});
const validateUser = req => {
  const schema = Joi.object({
    username: Joi.string()
      .min(3)
      .max(20)
      .required(),
    email: Joi.string()
      .email({ minDomainSegments: 2, tlds: { allow: ["-m"] } })
      .required(),
    password: Joi.string()
      .required()
      .min(8)
      .max(255)
  });
  return schema.validate(req);
};
userSchema.methods.generateAuthToken = function() {
  return jwt.sign(
    { _id: this._id, isAdmin: this.isAdmin },
    config.get("jwtPrivateKey")
  );
};
const User = mongoose.model("User", userSchema);

exports.validate = validateUser;
exports.User = User;
